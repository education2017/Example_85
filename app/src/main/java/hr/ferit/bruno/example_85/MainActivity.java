package hr.ferit.bruno.example_85;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {

    private static final int WAIT_TIME = 10000;
    private static final String TAG = "MainThread";

    @BindView(R.id.bRunOnMainThread) Button bRunOnMainThread;
    @BindView(R.id.bRunOnBackThread) Button bRunOnBackThread;
    @BindView(R.id.cbTestCheckBox) CheckBox cbResponsivnessTester;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.bRunOnBackThread, R.id.bRunOnMainThread})
    public void onButtonClick(Button button){
        switch (button.getId()){
            case R.id.bRunOnMainThread: repeatHeavyWorkMain(WAIT_TIME); break;
            case R.id.bRunOnBackThread: repeatHeavyWorkBack(WAIT_TIME); break;
        }
    }

    private void repeatHeavyWorkMain(int waitTime) {
            try { Thread.sleep(waitTime);}
            catch (InterruptedException e) { e.printStackTrace(); }
            Log.d(TAG, "Work done");
    }

    private void repeatHeavyWorkBack (int waitTime) {
            new MyCustomThread(waitTime).start();
    }
}

