package hr.ferit.bruno.example_85;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.util.Log;


/**
 * Created by Zoric on 29.8.2017..
 */

public class MyCustomThread extends Thread{

    private static final String TAG = "BackThread";
    private int mWaitTime;

    public MyCustomThread(int waitTime) {
        this.mWaitTime = waitTime;
    }

    @Override
    public void run() {
        try { Thread.sleep(this.mWaitTime); }
        catch (InterruptedException e) { e.printStackTrace(); }
        Log.d(TAG, "Work done");
    }
}
